import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-activity-info',
  templateUrl: './activity-info.component.html',
  styleUrls: ['./activity-info.component.scss']
})
export class ActivityInfoComponent implements OnInit {
  activities = [
    {
      id: 'bazen',
      name: 'BAZEN',
      text: `U vidu dodatnog treninga u ponudi imamo bazen koji je prevashodno namenjen potrebama dece. Obuka plivanja,
      korekcija i usavršavanje plivačkih tehnika, odvija se uz nadzor stručnih trenera koji svojim dugogodišnjim
      iskustvom i pedagoškim pristupom, omogućavaju svakom detetu da se opusti i oseća sigurno.`,
      videos: ['https://www.youtube.com/embed/zjmLsvldmJI', 'https://www.youtube.com/embed/CeZ3pP7AuKo'],
      cena: '',
      pictures: ['bazen/Activity_Centar1', 'bazen/Activity_Centar2', 'bazen/Activity_Centar3', 'bazen/Activity_Centar2',
        'bazen/Activity_Centar3', 'bazen/Activity_Centar4', 'bazen/Activity_Centar5', 'bazen/Activity_Centar6',
        'bazen/Activity_Centar7', 'bazen/Activity_Centar8', 'bazen/Activity_Centar9', 'bazen/Activity_Centar10',
        'bazen/Activity_Centar11', 'bazen/Activity_Centar12', 'bazen/Activity_Centar13', 'bazen/Activity_Centar14',
        'bazen/Activity_Centar15', 'bazen/Activity_Centar16', 'bazen/Activity_Centar17', 'bazen/Activity_Centar18',
        'bazen/Activity_Centar19']
    },
    {
      id: 'klizanje',
      name: 'KLIZANJE',
      text: `Treninzi klizanja se odvijaju na Igloo klizalištu na Olimpu u zatvorenom balonu na kome je podloga sintetički
      led koji je odličan za prve korake dece na klizaljkama. Trening ne zavisi od vremenskih uslova i deca u balonu
      uživaju na klizaljkama koje dobijaju od Školice.`,
      videos: null,
      cena: '',
      pictures: ['klizanje/Activity_Centar1', 'klizanje/Activity_Centar2']
    },
    {
      id: 'aerobik',
      name: 'AEROBIK',
      text: `Aerobik predstavlja dinamičan fitnes program, visokog intenziteta koji će vam omogućiti da u 60 minuta
      sagorite veliki broj kalorija. Efikasno utiče na: jačanje i toniziranje mišića, sagorevanje masnih naslaga,
      povećanje snage i izdržljivosti, oblikovanje celokupne muskulature i poboljšanje opšte fizičke kondicije.
      Svaki trening je drugačije osmišljen i time sprečava rutinu i jednoličnost. Kombinuju se različitih fitnes
      programi (u zavisnosti od dana u nedelji): hi-low, tae bo, step workout kao i korišćenje različitih rekvizita.
      Obavezan deo časa jeste zagrevanje čitave muskulature, odakle se prelazi na vežbe kojima aktiviramo više grupa
      mišića istovremeno, kroz veliki broj ponavljanja prvenstveno mišice gluteusa, nogu i stomaka.
      Najčesća kombinacija su čučnjevi ili iskoraci sa nekom od vežbi za ruke, nakon čega sledi intenzivan
      kardio deo, koji može biti koreografisan ili osmišljen kao hi impact trening sa skokovima, poskocima i trčanjem u mestu.
      Potom se prelazi na statične vežbe bez skakanja i vežbe za jačanje trbušnih i ledjnih mišica, i na kraju, rastezanje i opuštanje.`,
      dodatno: `Dinamika realizacije programa je tri puta nedeljno u sali, u trajanju 60 minuta. <br>
      Treninzi aerobika i zumbe se održavaju samo na lokaciji 1. <br>
      ulica Stevana Sinđelića 2 - ugao sa ulicom Kralja Petra prvog - Kaluđerica, Zvezdara, Beograd, Srbija`,
      videos: null,
      cena: '2000 mesečno',
      pictures: ['aerobik/Activity_Centar1', 'aerobik/Activity_Centar2', 'aerobik/Activity_Centar3', 'aerobik/Activity_Centar2',
        'aerobik/Activity_Centar3', 'aerobik/Activity_Centar4', 'aerobik/Activity_Centar5', 'aerobik/Activity_Centar6',
        'aerobik/Activity_Centar7', 'aerobik/Activity_Centar8', 'aerobik/Activity_Centar9']
    },
    {
      id: 'individualni/',
      name: 'INDIVIDUALNI TRENINZI',
      text: `Ukoliko ste odlučili da radite na poboljšanju svojih sposobnosti ili na promeni sopstvenog izgleda i lepoti, najbolje bi
      bilo da svoje vreme iskoristite na optimalan način. Preporučujemo Vam trening, osmišljen prema Vašim potrebama i sposobnostima,
      uz stalno prisustvo stručnog lica koje vodi računa o vašem treningu, motiviše vas, daje vam podršku i usmereno je ka ispunjenju
      Vaših ciljeva. U tu svrhu nudimo Vam opciju u kojoj vežbate u paru sa nekim vežbačem, ali i opciju pri kojoj ćete vežbati sami
      sa svojim trenerom. Takodje u ponudi imamo individualne treninge za decu`,
      dodatno: `Trener je osoba koja će vam pomoći da na najefektniji način iskoristite vreme za vežbanje, ali i da održite visok
      nivo lične motivacije tokom vežbanja. Bez obzira na vaše ranije iskustvo u vežbanju, trener ima zadatak da vas sigurno provede
      kroz sve elemente vežbanja unutar pojedinačno planiranih vežbi. <br>
      Kondicioni treninzi (opšti, usmereni) u zavisnosti od sportske grane. <br>
      Treninzi za gubljenje telesne mase.`,
      videos: null,
      cena: 'Dogovor sa trenerom',
      pictures: ['individualni/Activity_Centar1']
    },
    {
      id: 'rodjendani',
      name: 'SPORTSKI RODJENDANI',
      text: `Sportska animacija dece uzrasta od 1-7 godina predstavlja posebno osmišljen program baziran na aktivnostima koje prate
      kako interesovanja tako i psiho-motoričke mogućnosti dece određenog uzrasta. Program je interaktivan i predstavlja jednu vrstu
      "edukativne zabave". Veliki broj štafetnih igara i poligona spretnosti koji uključuju osnovne gimnastičke elemente, vežbe
      koordinacije pokreta i oblikovanja tela povezane su u jedno veliko sportsko nadmetanje prožeto muzičkim sadržajima.
      Ako ste voleli igre bez granica, onda znate kakvo je uzbudjenje menjati aktivnosti, nadmetati se, biti deo tima,
      koristiti i veštinu i snagu, a sjajno se zabavljati.`,
      dodatno: null,
      videos: null,
      cena: null,
      pictures: ['rodjendani/Activity_Centar1', 'rodjendani/Activity_Centar2', 'rodjendani/Activity_Centar3', 'rodjendani/Activity_Centar2',
        'rodjendani/Activity_Centar3', 'rodjendani/Activity_Centar4', 'rodjendani/Activity_Centar5', 'rodjendani/Activity_Centar6',
        'rodjendani/Activity_Centar7', 'rodjendani/Activity_Centar8', 'rodjendani/Activity_Centar9', 'rodjendani/Activity_Centar10',
        'rodjendani/Activity_Centar11', 'rodjendani/Activity_Centar12', 'rodjendani/Activity_Centar13', 'rodjendani/Activity_Centar14',
        'rodjendani/Activity_Centar15', 'rodjendani/Activity_Centar16', 'rodjendani/Activity_Centar17', 'rodjendani/Activity_Centar18',
        'rodjendani/Activity_Centar19', 'rodjendani/Activity_Centar20']
    },
  ];

  selected;

  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.selected = this.activities.filter((el) => {
      return el.id.toLowerCase() === this.route.snapshot.paramMap.get('name');
    });
  }

}
