import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-website',
  templateUrl: './website.component.html',
  styleUrls: ['./website.component.scss']
})
export class WebsiteComponent implements OnInit {
  @ViewChild('firstElement', { static: false }) firstElement: ElementRef;
  @ViewChild('secondElement', { static: false }) secondElement: ElementRef;
  // @ViewChild('thirdElement', { static: false }) thirdElement: ElementRef;
  @HostListener('window:scroll', ['$event']) // for window scroll events
  onScroll(event) {
    const scrollPosition = window.scrollY + window.innerHeight;
    const elementPosition = this.firstElement.nativeElement.getBoundingClientRect().top + window.scrollY
      + this.firstElement.nativeElement.clientHeight;
    console.log(scrollPosition, ' > ', elementPosition, this.firstElement.nativeElement.clientHeight);
    // const thirdElPos = this.thirdElement.nativeElement.getBoundingClientRect().top + window.scrollY
    //   + this.thirdElement.nativeElement.clientHeight;
    if (scrollPosition > elementPosition) {
      // console.log('animated', scrollPosition, elementPosition);
      this.addClass(this.firstElement);
    }
    // if (scrollPosition > thirdElPos) {
    //   this.addClass(this.thirdElement);
    // }
  }


  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  addClass(element) {
    element.nativeElement.className = 'animate__animated animate__flipInX animate__delay-2s';
    element.nativeElement.setAttribute('style', 'display: block;');
  }

  getElementPos(el) {
    return el.nativeElement.getBoundingClientRect().top + window.scrollY
      + el.nativeElement.clientHeight;
  }

  navigate(name) {
    this.router.navigate(['/website' + name]);
  }

}
