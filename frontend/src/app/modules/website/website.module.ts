import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WebsiteRoutingModule } from './website-routing.module';
import { WebsiteComponent } from './website.component';
import { ActivityPageComponent } from './components/activity-page/activity-page.component';
import { FooterComponent } from './components/footer/footer.component';
import { ActivityInfoComponent } from './components/activity-info/activity-info.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [WebsiteComponent, ActivityPageComponent, FooterComponent, ActivityInfoComponent],
  imports: [
    CommonModule,
    WebsiteRoutingModule,
    SharedModule
  ]
})
export class WebsiteModule { }
