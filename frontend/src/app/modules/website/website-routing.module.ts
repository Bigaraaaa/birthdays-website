import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActivityInfoComponent } from './components/activity-info/activity-info.component';
import { ActivityPageComponent } from './components/activity-page/activity-page.component';
import { WebsiteComponent } from './website.component';


const routes: Routes = [
  {
    path: '',
    children: [
      // { path: 'menu', component: AdminMenuComponent, canActivate: [AuthGuard], data: { expectedRoles: ['ROLE_ADMINISTRATOR']}},
      { path: 'activity-info/:name', component: ActivityInfoComponent },
      { path: 'activity', component: ActivityPageComponent },
      { path: 'home', component: WebsiteComponent },
      { path: '**', redirectTo: '/website/home', pathMatch: 'full' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WebsiteRoutingModule { }
