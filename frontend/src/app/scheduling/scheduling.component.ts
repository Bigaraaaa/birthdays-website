import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { FormErrorService } from '../shared/formError.service';

@Component({
  selector: 'app-scheduling',
  templateUrl: './scheduling.component.html',
  styleUrls: ['./scheduling.component.scss']
})
export class SchedulingComponent implements OnInit {

  public form: FormGroup;
  message = '';
  startDate = new FormControl(new Date());

  // temp

  bundles = ['Osnovni', 'Srednji', 'Full'];
  places = ['Novi Beograd', 'Karaburma', 'Zemun'];

  // temp

  constructor(private fb: FormBuilder, public readonly formError: FormErrorService) { }

  ngOnInit() {
    this.form = this.fb.group({
      firstName: ['', { validators: [Validators.pattern('[^0-9]{3,}')] }],
      lastName: ['', { validators: [Validators.pattern('[^0-9]{3,}')] }],
      phone: ['', { validators: [Validators.required, Validators.pattern('[0-9+ ]{3,}')] }],
      email: ['', {}],
      description: ['', {}],
      startDate: [new Date(), { validators: [Validators.required] }],
      bundle: ['', { validators: [Validators.required] }],
      place: ['', { validators: [Validators.required] }]
    });
  }

  onRegister() { }

}
