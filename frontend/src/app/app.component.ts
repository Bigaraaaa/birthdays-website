import { TagContentType } from '@angular/compiler';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'birthday-website';
  @ViewChild('loader', { static: false }) loader: ElementRef;
  @ViewChild('myVideo', { static: false }) myVideo: ElementRef;

  constructor(
    private router: Router
  ) { }

  videoEnded() {
    this.loader.nativeElement.setAttribute('style', 'display: none;');
  }

  goHome() {
    this.router.navigate(['/website/home']);
  }

  navigate(name) {
    this.router.navigate(['/website' + name]);
  }

}
