import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActivityPageComponent } from './modules/website/components/activity-page/activity-page.component';
import { WebsiteComponent } from './modules/website/website.component';


const routes: Routes = [
  // { path: '', component: ActivityPageComponent },
  // { path: '**', component: WebsiteComponent, pathMatch: 'full' }
  {
    path: 'website',
    loadChildren: './modules/website/website.module#WebsiteModule'
  },
  {
    path: '**',
    redirectTo: '/website/home'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
