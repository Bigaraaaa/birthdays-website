package app.utils;

public class View {
    public interface HideOptionalProperties {};
    public interface ShowItem{};
    public interface ShowBundle{};
    public interface ShowPlace{};
    public interface ShowPurchase{};
}
