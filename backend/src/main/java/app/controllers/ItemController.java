package app.controllers;

import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import app.models.Item;
import app.services.ItemService;
import app.utils.View.HideOptionalProperties;

@RestController
@RequestMapping("/item")
public class ItemController {

	@Autowired
	ItemService itemService;
    
    @JsonView(HideOptionalProperties.class)
    @RequestMapping()
    public ResponseEntity<Iterable<Item>> getItems() {
        return new ResponseEntity<Iterable<Item>>(itemService.getItems(), HttpStatus.OK);
    }

    @JsonView(HideOptionalProperties.class)
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public ResponseEntity<Item> getItemById(@PathVariable Long id) {
        Optional<Item> otp_item = itemService.getItemById(id);
        if(otp_item.isPresent()) {
            return new ResponseEntity<Item>(otp_item.get(), HttpStatus.OK);
        }
        return new ResponseEntity<Item>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value="", method=RequestMethod.POST)
    public ResponseEntity<Item> addItem(@RequestBody Item item) {
        itemService.addItem(item);
        return new ResponseEntity<Item>(item, HttpStatus.CREATED);
    }

    @RequestMapping(value="/{id}", method=RequestMethod.PUT)
    public ResponseEntity<Item> updateItem(@PathVariable Long id, @RequestBody Item item) {
        itemService.updateItem(id, item);
        return new ResponseEntity<Item>(item, HttpStatus.CREATED);
    }

    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<Item> removeItem(@PathVariable Long id) {
        try {
            itemService.removeItem(id);
        }catch (Exception e) {
            return new ResponseEntity<Item>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<Item>(HttpStatus.NO_CONTENT);
    }
}
