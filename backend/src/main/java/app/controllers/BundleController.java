package app.controllers;

import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import app.models.Bundle;
import app.services.BundleService;
import app.utils.View.HideOptionalProperties;

@RestController
@RequestMapping("/bundle")
public class BundleController {

	@Autowired
    BundleService bundleService;
    
    @JsonView(HideOptionalProperties.class)
    @RequestMapping()
    public ResponseEntity<Iterable<Bundle>> getBundles() {
        return new ResponseEntity<Iterable<Bundle>>(bundleService.getBundles(), HttpStatus.OK);
    }

    @JsonView(HideOptionalProperties.class)
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public ResponseEntity<Bundle> getBundleById(@PathVariable Long id) {
        Optional<Bundle> opt_bundle = bundleService.getBundleById(id);
        if(opt_bundle.isPresent()) {
            return new ResponseEntity<Bundle>(opt_bundle.get(), HttpStatus.OK);
        }
        return new ResponseEntity<Bundle>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value="", method=RequestMethod.POST)
    public ResponseEntity<Bundle> addBundle(@RequestBody Bundle bundle) {
        bundleService.addBundle(bundle);
        return new ResponseEntity<Bundle>(bundle, HttpStatus.CREATED);
    }

    @RequestMapping(value="/{id}", method=RequestMethod.PUT)
    public ResponseEntity<Bundle> updateBundle(@PathVariable Long id, @RequestBody Bundle bundle) {
        bundleService.updateBundle(id, bundle);
        return new ResponseEntity<Bundle>(bundle, HttpStatus.CREATED);
    }

    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<Bundle> removeBundle(@PathVariable Long id) {
        try {
            bundleService.removeBundle(id);
        }catch (Exception e) {
            return new ResponseEntity<Bundle>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<Bundle>(HttpStatus.NO_CONTENT);
    }
	
}
