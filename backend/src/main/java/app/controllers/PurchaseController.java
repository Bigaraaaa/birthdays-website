package app.controllers;

import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import app.models.Purchase;
import app.services.PurchaseService;
import app.utils.View.HideOptionalProperties;

@RestController
@RequestMapping("/purchase")
public class PurchaseController {
	
	@Autowired
	PurchaseService purchaseService;
    
    @JsonView(HideOptionalProperties.class)
    @RequestMapping()
    public ResponseEntity<Iterable<Purchase>> getPurchases() {
        return new ResponseEntity<Iterable<Purchase>>(purchaseService.getPurchases(), HttpStatus.OK);
    }

    @JsonView(HideOptionalProperties.class)
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public ResponseEntity<Purchase> getPurchaseById(@PathVariable Long id) {
        Optional<Purchase> opt_purchases = purchaseService.getPurchaseById(id);
        if(opt_purchases.isPresent()) {
            return new ResponseEntity<Purchase>(opt_purchases.get(), HttpStatus.OK);
        }
        return new ResponseEntity<Purchase>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value="", method=RequestMethod.POST)
    public ResponseEntity<Purchase> addPurchase(@RequestBody Purchase purchase) {
        purchaseService.addPurchase(purchase);
        return new ResponseEntity<Purchase>(purchase, HttpStatus.CREATED);
    }

    @RequestMapping(value="/{id}", method=RequestMethod.PUT)
    public ResponseEntity<Purchase> updatePurchase(@PathVariable Long id, @RequestBody Purchase purchase) {
        purchaseService.updatePurchase(id, purchase);
        return new ResponseEntity<Purchase>(purchase, HttpStatus.CREATED);
    }

    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<Purchase> removePurchase(@PathVariable Long id) {
        try {
            purchaseService.removePurchase(id);
        }catch (Exception e) {
            return new ResponseEntity<Purchase>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<Purchase>(HttpStatus.NO_CONTENT);
    }
	
}
