package app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import app.models.User;
import app.services.UserService;

@RestController
@RequestMapping("/user")
public class UserController {
    
    @Autowired
	UserService userService;

    @RequestMapping(value="", method=RequestMethod.POST)
    public ResponseEntity<User> addUser(@RequestBody User user) {
        userService.addUser(user);
        return new ResponseEntity<User>(user, HttpStatus.CREATED);
    }

}
