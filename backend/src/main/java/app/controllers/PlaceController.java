package app.controllers;

import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import app.models.Place;
import app.services.PlaceService;
import app.utils.View.HideOptionalProperties;

@RestController
@RequestMapping("/place")
public class PlaceController {
	
	@Autowired
	PlaceService placeService;
    
    @JsonView(HideOptionalProperties.class)
    @RequestMapping()
    public ResponseEntity<Iterable<Place>> getPlaces() {
        return new ResponseEntity<Iterable<Place>>(placeService.getPlaces(), HttpStatus.OK);
    }

    @JsonView(HideOptionalProperties.class)
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public ResponseEntity<Place> getPlaceById(@PathVariable Long id) {
        Optional<Place> otp_place = placeService.getPlaceById(id);
        if(otp_place.isPresent()) {
            return new ResponseEntity<Place>(otp_place.get(), HttpStatus.OK);
        }
        return new ResponseEntity<Place>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value="", method=RequestMethod.POST)
    public ResponseEntity<Place> addPlace(@RequestBody Place place) {
        placeService.addPlace(place);
        return new ResponseEntity<Place>(place, HttpStatus.CREATED);
    }

    @RequestMapping(value="/{id}", method=RequestMethod.PUT)
    public ResponseEntity<Place> updatePlace(@PathVariable Long id, @RequestBody Place place) {
        placeService.updatePlace(id, place);
        return new ResponseEntity<Place>(place, HttpStatus.CREATED);
    }

    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<Place> removePlace(@PathVariable Long id) {
        try {
            placeService.removePlace(id);
        }catch (Exception e) {
            return new ResponseEntity<Place>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<Place>(HttpStatus.NO_CONTENT);
    }
	
}
