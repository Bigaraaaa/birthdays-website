package app.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import app.repositories.UserRepository;
import app.models.User;

@Service
public class UserService {
    
    @Autowired
    private UserRepository userRep;

    @Autowired
	private PasswordEncoder passwordEncoder;

    @Secured("ROLE_ADMIN")
    public void addUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
    	userRep.save(user);
    }

    public Optional<User> getUserByUsername(String username) {
        return userRep.findByUsername(username);
    }
}
