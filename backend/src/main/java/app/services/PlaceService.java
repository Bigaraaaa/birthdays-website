package app.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.models.Place;
import app.repositories.PlaceRepository;

@Service
public class PlaceService {
	
	@Autowired
    private PlaceRepository placeRepo;
	

    public Iterable<Place> getPlaces() {
        return placeRepo.findAll();
    }

    public Optional<Place> getPlaceById(Long id) {
        return placeRepo.findById(id);
    }

    public void addPlace(Place place) {
    	placeRepo.save(place);
    }

    public void removePlace(Long id) {
        Optional<Place> otp_place = placeRepo.findById(id);
        placeRepo.delete(otp_place.get());
    }

    public void updatePlace(Long id, Place place) {
        Optional<Place> otp_place = placeRepo.findById(id);
        if(otp_place.isPresent()) {
        	place.setId(otp_place.get().getId());
            placeRepo.save(place);
        }
    }
	
}
