package app.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.models.Item;
import app.repositories.ItemRepository;

@Service
public class ItemService {
	
	@Autowired
    private ItemRepository itemRepo;
	
	public Iterable<Item> getItems() {
        return itemRepo.findAll();
    }

    public Optional<Item> getItemById(Long id) {
        return itemRepo.findById(id);
    }

    public void addItem(Item item) {
    	itemRepo.save(item);
    }

    public void removeItem(Long id) {
        Optional<Item> opt_item = itemRepo.findById(id);
        itemRepo.delete(opt_item.get());
    }

    public void updateItem(Long id, Item item) {
        Optional<Item> opt_item = itemRepo.findById(id);
        if(opt_item.isPresent()) {
        	item.setId(opt_item.get().getId());
            itemRepo.save(item);
        }
    }
	
}
