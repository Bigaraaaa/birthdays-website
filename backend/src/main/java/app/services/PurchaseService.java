package app.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.models.Purchase;
import app.repositories.PurchaseRepository;

@Service
public class PurchaseService {
	@Autowired
    private PurchaseRepository purchaseRepo;
	

    public Iterable<Purchase> getPurchases() {
        return purchaseRepo.findAll();
    }

    public Optional<Purchase> getPurchaseById(Long id) {
        return purchaseRepo.findById(id);
    }

    public void addPurchase(Purchase purchase) {
    	purchaseRepo.save(purchase);
    }

    public void removePurchase(Long id) {
        Optional<Purchase> opt_puchase = purchaseRepo.findById(id);
        purchaseRepo.delete(opt_puchase.get());
    }

    public void updatePurchase(Long id, Purchase purchase) {
        Optional<Purchase> opt_puchase = purchaseRepo.findById(id);
        if(opt_puchase.isPresent()) {
        	purchase.setId(opt_puchase.get().getId());
            purchaseRepo.save(purchase);
        }
    }
}
