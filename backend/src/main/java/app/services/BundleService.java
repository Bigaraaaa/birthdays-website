package app.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.models.Bundle;
import app.repositories.BundleRepository;

@Service
public class BundleService {
	
	@Autowired
    private BundleRepository bundleRepo;


    public Iterable<Bundle> getBundles() {
        return bundleRepo.findAll();
    }

    public Optional<Bundle> getBundleById(Long id) {
        return bundleRepo.findById(id);
    }

    public void addBundle(Bundle bundle) {
    	bundleRepo.save(bundle);
    }

    public void removeBundle(Long id) {
        Optional<Bundle> opt_bundle = bundleRepo.findById(id);
        bundleRepo.delete(opt_bundle.get());
    }

    public void updateBundle(Long id, Bundle bundle) {
        Optional<Bundle> opt_bundle = bundleRepo.findById(id);
        if(opt_bundle.isPresent()) {
        	bundle.setId(opt_bundle.get().getId());
            bundleRepo.save(bundle);
        }
    }
}
