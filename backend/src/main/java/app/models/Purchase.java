package app.models;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;

@Entity
public class Purchase {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
	private Date start_date;
	
	@Column
	private String email;
	
	@Column
	private String firstname;
	
	@Column
	private String lastname;
	
	@Column
	private String description;
	
	@Column(nullable = false)
	private Integer phone;

	@OneToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	private Bundle bundle;

	public Purchase() {
		
	}
	
	public Purchase(Date start_date, String email, String firstname, String lastname, String description, Integer phone,
			Bundle bundle) {
		this.start_date = start_date;
		this.email = email;
		this.firstname = firstname;
		this.lastname = lastname;
		this.description = description;
		this.phone = phone;
		this.bundle = bundle;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getPhone() {
		return phone;
	}

	public void setPhone(Integer phone) {
		this.phone = phone;
	}

	public Bundle getBundle() {
		return bundle;
	}

	public void setBundle(Bundle bundle) {
		this.bundle = bundle;
	}
}
