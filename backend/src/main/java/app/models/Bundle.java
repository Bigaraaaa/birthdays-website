package app.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonView;

import app.utils.View.ShowPurchase;

@Entity
public class Bundle {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name = "bundle_id")
	private Set<Item> items;
	
	@OneToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	private Place place;
	
	@Column
	private Integer price;
	
	@JsonView(ShowPurchase.class)
	@OneToOne(mappedBy = "bundle")
	private Purchase purchase;

	public Bundle() {
		
	}
	
	public Bundle(Set<Item> items, Place place, Integer price, Purchase purchase) {
		this.items = items;
		this.place = place;
		this.price = price;
		this.purchase = purchase;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<Item> getItems() {
		return items;
	}

	public void setItems(Set<Item> items) {
		this.items = items;
	}

	public Place getPlace() {
		return place;
	}

	public void setPlace(Place place) {
		this.place = place;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Purchase getPurchase() {
		return purchase;
	}

	public void setPurchase(Purchase purchase) {
		this.purchase = purchase;
	}	
}
