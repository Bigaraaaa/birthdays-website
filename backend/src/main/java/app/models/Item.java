package app.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonView;

import app.utils.View.ShowBundle;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;

@Entity
public class Item {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@JsonView(ShowBundle.class)
	@ManyToOne
	@JoinColumn(name = "bundle_id")
	private Bundle bundle;
	
	@Column(length=128, nullable = false)
	private String price;
	
	@Column(length=128, nullable = false)
	private String name;
	
	@Column
	@ElementCollection
	private Set<String> images;

	public Item() {
		
	}
	
	public Item(Bundle bundle, String price, String name, Set<String> images) {
		this.bundle = bundle;
		this.price = price;
		this.name = name;
		this.images = images;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<String> getImages() {
		return images;
	}

	public void setImages(Set<String> images) {
		this.images = images;
	}

	public Bundle getBundle() {
		return bundle;
	}

	public void setBundle(Bundle bundle) {
		this.bundle = bundle;
	}
}
