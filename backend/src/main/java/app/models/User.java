package app.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class User {
    
    @Id
	  @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    
    @Column(length=128, nullable = false)
    private String username;
    
    @Column(nullable = false)
    private String password;

    @Column(length=128, nullable = false)
    private String permission;
    
    User(){

    }

    User(String username, String password){
        this.username = username;
        this.password = password;
    }

    public Long getId() {
		  return id;
	  }

    public void setId(Long id) {
		  this.id = id;
  	}

	  public String getUsename() {
		  return username;
    }
    
    public void setUsername(String username) {
		  this.username = username;
	  }

    public String getPassword() {
		  return password;
    }
    
    public void setPassword(String password) {
		  this.password = password;
	  }

    public String getPermission() {
		  return permission;
    }
    
    public void setParmission(String permission) {
		  this.permission = permission;
  	}
    
}
