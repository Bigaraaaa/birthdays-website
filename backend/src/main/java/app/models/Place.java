package app.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonView;

import app.utils.View.ShowBundle;

import java.util.Set;
import java.util.HashMap;

import javax.persistence.Column;
import javax.persistence.ElementCollection;

@Entity
public class Place {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(length=128, nullable = false)
	private String name;
	
	@Column
	private HashMap<String, String> location;
	
	@Column
	@ElementCollection
	private Set<String> images;
	
	@JsonView(ShowBundle.class)
	@OneToOne(mappedBy = "place")
	private Bundle bundle;

	public Place() {
		
	}
	
	public Place(String name, HashMap<String, String> location, Set<String> images, Bundle bundle) {
		this.name = name;
		this.location = location;
		this.images = images;
		this.bundle = bundle;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public HashMap<String, String> getLocation() {
		return location;
	}

	public void setLocation(HashMap<String, String> location) {
		this.location = location;
	}

	public Set<String> getImages() {
		return images;
	}

	public void setImages(Set<String> images) {
		this.images = images;
	}

	public Bundle getBundle() {
		return bundle;
	}

	public void setBundle(Bundle bundle) {
		this.bundle = bundle;
	}
}
