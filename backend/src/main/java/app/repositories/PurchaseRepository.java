package app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import app.models.Purchase;

@Repository
public interface PurchaseRepository extends JpaRepository<Purchase, Long> {

}
