package app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import app.models.Place;

@Repository
public interface PlaceRepository extends JpaRepository<Place, Long> {

}
